family=binomial(link='cloglog'),
data=HawCon)
aic2.cll[i] <- AIC(zcll2)
}
gph1 <- xyplot(aic.cll~aic.logit,
key=list(text=list(c("logit model","cll model", "Data")),
points=list(pch=c(1,1,2),
col=c('blue','red','black')),columns=3))
gph2 <- xyplot(aic2.cll~aic2.logit, col='red')
gph3 <- gph1+latticeExtra::as.layer(gph2)+
latticeExtra::layer(panel.abline(0,1),
panel.points(aicData[['logit']],
aicData[['cll']], pch=2, cex=2, col=1))
update(gph3, xlim=range(c(aic.logit, aic2.logit,
aicData['logit']))*c(.98,1.02),
ylim=range(c(aic.cll, aic2.cll,
aicData['cll']))*c(.98,1.02))
cap5 <- "Diagnostics for model with binomial errors and observation level
random effects."
set.seed(29)
simRefcll <- suppressWarnings(
DHARMa::simulateResiduals(HCbiObs.cll, n=250, seed=FALSE) )
DHARMa::plotResiduals(simRefcll, xlab='cll: model prediction')
DHARMa::plotResiduals(simRefcll, form=log(HawCon$Total),
xlab="cll: log(Total)")
simReflogit <- suppressWarnings(
DHARMa::simulateResiduals(HCbiObs.logit, n=250, seed=FALSE) )
DHARMa::plotResiduals(simReflogit, xlab='logit: model prediction')
DHARMa::plotResiduals(simReflogit, form=log(HawCon$Total),
xlab="logit: log(Total)")
shorten <- function(nam, leaveout=c('trtGp','Fly',':')){
for(txt in leaveout){
nam <- gsub(txt,'', nam, fixed=TRUE)
}
nam
}
## Fit two simplistic and unsatisfactory models.
HCbbDisp1.cll <- update(HCbb.cll, dispformula=~1)
HCbin.cll <- update(HCbb.cll, family=binomial(link="cloglog"))
LTbb.cll <- qra::extractLT(p=0.99, obj=HCbb.cll, link="cloglog",
a=1:8, b=9:16, eps=0, df.t=NULL)[,-2]
rownames(LTbb.cll) <- shorten(rownames(LTbb.cll))
## NB: The formula has used the scaled value of time.
## Below, `offset` is used to retrieve the scaling parameters
## `center` ## and `scale` in `(x-center)/scale`.
offset <- qra::getScaleCoef(HawCon$scTime)
LTbiObs.cll <- qra::extractLT(p=0.99, obj=HCbiObs.cll,
a=1:8, b=9:16, eps=0, offset=offset,
df.t=NULL)[,-2]
rownames(LTbiObs.cll) <- shorten(rownames(LTbiObs.cll))
LTbb.logit <- qra::extractLT(p=0.99, obj=HCbb.logit, link="logit",
a=1:8, b=9:16, eps=0, offset=0,
df.t=NULL)[,-2]
rownames(LTbb.logit) <- shorten(rownames(LTbb.logit))
LTbbDisp1.cll <-
qra::extractLT(p=0.99, obj=HCbbDisp1.cll,
a=1:8, b=9:16, eps=0, df.t=NULL)[,-2]
rownames(LTbbDisp1.cll) <- shorten(rownames(LTbbDisp1.cll))
LTbin.cll <-
qra::extractLT(p=0.99, obj=HCbin.cll,
a=1:8, b=9:16, eps=0, df.t=NULL)[,-2]
rownames(LTbin.cll) <- shorten(rownames(LTbin.cll))
OKplotrix <- requireNamespace("plotrix", quietly = TRUE)
if (!OKplotrix) {
message("This vignette requires a package not available/installed: ")
message("plotrix")
message("Code that requires this package will not be executed.")
}
cap9 <- "LT99 $95\\%$ confidence intervals are compared between
five different models."
gpNam <- rownames(LTbb.cll)
ordEst <- order(LTbb.cll[,1])
col5 <- c("blue","lightslateblue","brown",'gray','gray50')
plotrix::plotCI(1:8-0.34, y=LTbb.cll[ordEst,1], ui=LTbb.cll[ordEst,3],
li=LTbb.cll[ordEst,2], lwd=2, col=col5[1], xaxt="n",
fg="gray", xlab="", ylab="LT99 Estimate (days)",
xlim=c(0.8,8.2), ylim=c(0,29), sfrac=0.008)
plotrix::plotCI(1:8-0.17, y=LTbiObs.cll[ordEst,1], ui=LTbiObs.cll[ordEst,3],
li=LTbiObs.cll[ordEst,2], lwd=2, col=col5[2], xaxt="n",
fg="gray", xlab="", ylab="LT99 Estimate (days)",
xlim=c(0.8,8.2), ylim=c(0,29), add=TRUE, sfrac=0.008)
plotrix::plotCI(1:8, y=LTbb.logit[ordEst,1], ui=LTbb.logit[ordEst,3],
li=LTbb.logit[ordEst,2], lwd=2, col=col5[3], xaxt="n",
add=TRUE, sfrac=0.008)
plotrix::plotCI(1:8+0.17, y=LTbbDisp1.cll[ordEst,1], ui=LTbbDisp1.cll[ordEst,3],
li=LTbbDisp1.cll[ordEst,2], lwd=2, col=col5[4], xaxt="n",
add=TRUE, sfrac=0.008)
plotrix::plotCI(1:8+0.34, y=LTbin.cll[ordEst,1], ui=LTbin.cll[ordEst,3],
li=LTbin.cll[ordEst,2], lwd=2, col=col5[5], xaxt="n",
add=TRUE, sfrac=0.008)
axis(1, at=1:8, labels=gpNam[ordEst], las=2, lwd=0,
lwd.ticks=0.5, col.ticks="gray")
legend("topleft", legend=c("BB-cll (cll=cloglog)",  "BB-cll-ObsRE", "BB-logit",
"BB-cll, const Disp factor",
"Binomial-cll"),
inset=c(0.01,0.01), lty=c(1,1), col=col5[1:5],
text.col=col5[1:5], bty="n",y.intersp=0.85)
## Comparisons using `glmer()`
HCglmerBIobs.cll <-
lme4::glmer(cbind(Dead, Live) ~ 0 + trtGp/scTime + (1|obs) + (1|trtGpRep),
family=binomial(link='cloglog'), nAGQ=1, data=HawCon,
control=lme4::glmerControl(optimizer='bobyqa'))
HCglmerBIobs2s.cll <- suppressWarnings(
lme4::glmer(cbind(Dead, Live) ~ 0 + trtGp/scTime + splines::ns(scTime,2) +
(1|obs) + (1|trtGpRep), family=binomial(link='cloglog'),
nAGQ=0, data=HawCon))
HCglmerBIobs.logit <- lme4::glmer(cbind(Dead, Live) ~ 0 + trtGp/scTime +
(1|obs) + (1|trtGpRep),
family=binomial(link='logit'), nAGQ=0, data=HawCon,
control=glmerControl(optimizer='bobyqa'))
HCglmerBIobs2s.logit <-
lme4::glmer(cbind(Dead, Live) ~ 0 + trtGp/scTime + splines::ns(scTime,2) +
(1|obs) + (1|trtGpRep), family=binomial(link='logit'), nAGQ=0,
data=HawCon, control=glmerControl(optimizer='bobyqa'))
cfAIC <-
AIC(HCbiObs.cll,HCglmerBIobs.cll,HCbiObs.logit,HCglmerBIobs.logit,
HCglmerBIobs2s.cll,HCglmerBIobs2s.logit)
rownames(cfAIC) <- c('TMB:cll','mer:cll','TMB:lgt','mer:lgt',
'mer:cllCurve', 'mer:lgtCurve')
(tab <- t(round(cfAIC,2)))
cloglog <- make.link('cloglog')$linkfun
HCgauss.cll <- glmmTMB::glmmTMB(cloglog((PropDead+0.002)/1.004)~0+
trtGp/TrtTime+(TrtTime|trtGpRep),
family=gaussian(), data=HawCon)
LTgauss.cll <- qra::extractLT(p=0.99, obj=HCgauss.cll, link="cloglog",
a=1:8, b=9:16, eps=0.002, offset=c(0,1),                                 df.t=NULL)[,-2]
rownames(LTgauss.cll) <- shorten(rownames(LTgauss.cll))
cap11 <- "Residuals versus predicted quantile deviations, for
the linear mixed model,
with \\(log(1-log((p+0.002)/(1+0.004)))\\) as the dependent
variable, complementary log-log link, and gaussian error."
sim <- DHARMa::simulateResiduals(HCgauss.cll)
DHARMa::plotResiduals(sim)
cap12 <- "Comparison of estimates and
upper and lower $95\\%$ confidence limits, between the
preferred betabinomial complementary log-log model and this
model."
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace(kableExtra)){
knitr::kable(tab, booktabs=TRUE, linesep=linesep, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) %>%
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, linesep=linesep, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) %>%
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, linesep=linesep, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) !>
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, linesep=linesep, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
?kableExtra::kable_styling
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, htmltable_class='lightable_striped', font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
knitr::kable(tab, booktabs=TRUE, format='html', caption=cap12,
format.args=list(justify="right", width=9)) |>
kableExtra::kable_styling("striped", stripe_index = 5:8, htmltable_class='lightable_classic', font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
?kableExtra::kable_paper
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
kableExtra::kbl(tab, caption=cap12) |>
kableExtra::kable_paper("striped", font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T)
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
kableExtra::kbl(tab, caption=cap12)
kableExtra::kbl(tab, caption=cap12) |>
kableExtra::kable_paper("striped", font_size=9, full_width=FALSE)
kableExtra::kbl(tab, caption=cap12) |>
kableExtra::kable_paper("striped", font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE)
kableExtra::kbl(tab, caption=cap12) |>
kableExtra::kable_paper("striped", font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
kableExtra::kbl(tab, caption=cap12) |>
kableExtra::kable_paper("striped", font_size=9, full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
if(requireNamespace('kableExtra')){
kableExtra::kbl(tab, font_size=9, caption=cap12) |>
kableExtra::kable_paper("striped", full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
?'&'
if(T)if(T)print(1)
if(T)if(F)print(1)
if(T)if(F)print(1)else print(2)
?ifelse
packageVersion('a')
if(require('a')&package_version(a)>-"1.0")
)
if(require('a')&package_version(a)>-"1.0") print(1) else print(2)
if(require('a')&&package_version(a)>-"1.0") print(1) else print(2)
if(require('a')&package_version(a)>-"1.0") print(1) else print(2) else print(3)
if(require('stats')&package_version('a')>-"1.0") print(1) else print(2) else print(3)
if(require('stats')) if(package_version('stats')>="1.0" print(1) else print(2)
if(require('stats')) if(package_version('stats')>="1.0") print(1) else print(2)
if(require('lme')) if(package_version('lme')>="1.0") print(1) else print(2)
if(require('lme4')) if(package_version('lme4')>="1.0") print(1) else print(2)
package_version('lme4')
packageVersion('lme4')
?package_version
if(require('lme4')) if(packageVersion('lme4')>="1.0") print(1) else print(2)
if(require('lme4')) if(packageVersion('lme4')>="6.0") print(1) else print(2)
if(require('lme')) if(packageVersion('lme')>="6.0") print(1) else print(2)
if(require('lme', warning=F)) if(packageVersion('lme')>="6.0") print(1) else print(2)
args(require)
if(require('lme', quietly=T)) if(packageVersion('lme')>="6.0") print(1) else print(2)
?suppressPackageStartupMessages
args(test)
args(testit)
z <- suppressPackageStartupMessages(require('TMB'))
z
z <- suppressPackageStartupMessages(require('a'))
z <- suppressPackageStartupMessages(require('a',warning=F))
z <- suppressWarnings(require('a'))
z <- suppressWarnings(require('a', quietly=T))
req_namspace <- "glmmTMB"
pcheck <- suppressWarnings(requireNamespace(req_namspace, quietly = TRUE))
if(pcheck) pcheck & packageVersion("glmmTMB") >= "1.1.2"
if(!pcheck){
message("`glmmTMB` version >= 1.1.2 is not available")
message("Will not continue execution of vignette")
knit_exit()
}
req_namspace <- 'a'
pcheck <- suppressWarnings(requireNamespace(req_namspace, quietly = TRUE))
if(pcheck) pcheck & packageVersion("glmmTMB") >= "1.1.2"
if(!pcheck){
message("`glmmTMB` version >= 1.1.2 is not available")
message("Will not continue execution of vignette")
knit_exit()
}
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('bb',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
pcheck <- suppressWarnings(requireNamespace("kableExtra", quietly = TRUE))
if(pcheck) pcheck & packageVersion("kableExtra") >= "1.2"
if(pcheck){
kableExtra::kbl(tab, font_size=9, caption=cap12) |>
kableExtra::kable_paper("striped", full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='r')
} else print(tab)
cfLTs <- cbind(LTbb.cll, LTgauss.cll)
colnames(cfLTs) <- c(rep('BB',3),rep('gauss',3))
tab <- round(cfLTs[,c(c(1,4),c(1,4)+1,c(1,4)+2)],1)
pcheck <- suppressWarnings(requireNamespace("kableExtra", quietly = TRUE))
if(pcheck) pcheck & packageVersion("kableExtra") >= "1.2"
if(pcheck){
kableExtra::kbl(tab, font_size=9, caption=cap12) |>
kableExtra::kable_paper("striped", full_width=FALSE) |>
kableExtra::column_spec(6:7, bold=TRUE) |>
kableExtra::row_spec(5:8, color='blue', bold=T) |>
kableExtra::add_header_above(c(' '=1,'Estimate'=2,'Lower'=2, 'Upper'=2), align='c')
} else print(tab)
pcheck <- suppressMessages(requireNamespace('DHARMa', quietly = TRUE))
yesDHARMa <- !pcheck
if (!yesDHARMa) {
message("The suggested package `DHARMa` is not available/installed.")
message("Code that requires this package will not be executed.")
}
pcheck
search()
library(gamlss)
message("Earlier versions of `glmmTMB` may have issues for\n matching versions of `TMB` and `Matrix`")
?req_namspace
library(qra)
rhub::platforms()
rhub::platforms()[9,]
?rhub::check
rhub::check(show_status=F)
form <- cbind(dead,total-dead)~0+Cultivar/dose+(1|cultRep)
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
dispformula=~0+Cultivar/splines::ns(dose,2),
data=subset(codling1989,dose>0))
library(qra)
form <- cbind(dead,total-dead)~0+Cultivar/dose+(1|cultRep)
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
dispformula=~0+Cultivar/splines::ns(dose,2),
data=subset(codling1989,dose>0))
form <- cbind(dead,total-dead)~0+Cultivar/dose+(1|cultRep)
codling1989[1:4,]
table(codling1989$dose)
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
dispformula=~0+Cultivar/splines::ns(dose,2),
data=subset(codling1989,dose>8))
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
data=subset(codling1989,dose>8))
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
data=subset(codling1989,dose>0))
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
dispformula=~0+Cultivar/poly(dose,2),
data=subset(codling1989,dose>8))
knitr::opts_chunk$set(echo = FALSE, comment=NA, width=70)
options(show.signif.stars=FALSE)
if("VGAM" %in% (.packages()))
detach("package:VGAM", unload=TRUE)
# Load packages that will be used
suppressMessages({library(lme4); library(splines)})
form <- cbind(dead,total-dead)~0+Cultivar/dose+(1|cultRep)
codling1989.TMB <- glmmTMB::glmmTMB(formula=form,
family=glmmTMB::betabinomial(link='cloglog'),
data=subset(codling1989,dose>8))
HCbb2s.cll <- update(HCbb.cll, formula=form2s)
if("VGAM" %in% (.packages()))
detach("package:VGAM", unload=TRUE)
# Load packages that will be used
suppressMessages({library(lme4); library(splines)})
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
form2s <- cbind(Dead,Live)~0+trtGp/TrtTime+ns(scTime,2)+(1|trtGpRep)
HCbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawCon)
HCbb2s.cll <- update(HCbb.cll, formula=form2s)
HCbb.logit <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(TrtTime,2),
family=glmmTMB::betabinomial(link="logit"),
data=HawCon)
HCbb2s.logit <- update(HCbb.logit, formula=form2s)
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
HCbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawCon)
LTbb.cll <- qra::extractLT(p=0.99, obj=HCbb.cll, link="cloglog",
a=1:8, b=9:16, eps=0, df.t=NULL)[,-2]
form
table(HawCon$trtGp)
HawCon[1:2,]
HawMed <- droplevels(subset(HawCon, CN=="MedFly"))
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
HCbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawMed)
qra::extractLT(p=0.99, obj=HCbb.cll, link="cloglog",
a=1:4, b=5:8, eps=0, df.t=NULL)[,-2]
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
HawMedbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawMed)
qra::extractLT(p=0.99, obj=HawMedbb.cll, link="cloglog",
a=1:4, b=5:8, eps=0, df.t=NULL)[,-2]
devtools::document()
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
> HawMedbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
+                              family=glmmTMB::betabinomial(link="cloglog"),
+                              data=HawMed)
> qra::extractLT(p=0.99, obj=HawMedbb.cll, link="cloglog",
+                                                        a=1:4, b=5:8, eps=0, df.t=NULL)[,-2]
devtools::document()
HawMed[1:4,]
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
HawMed <- droplevels(subset(HawCon, CN=="MedFly"))
HawMedbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawMed)
splines::ns
names(HawMed)
qra::HawCon[1:3,]
devtools::document()
form <- cbind(Dead,Live)~0+trtGp/TrtTime+(1|trtGpRep)
HawMed <- droplevels(subset(HawCon, CN=="MedFly"))
HawMed <- within(HawMed,
{trtGp <- factor(paste0(CN,LifestageTrt, sep=":"))
trtGpRep <- paste0(CN,LifestageTrt,":",RepNumber)
scTime <- scale(TrtTime) })
HawMedbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawMed)
round(qra::extractLT(p=0.99, obj=HawMedbb.cll, link="cloglog",
a=1:4, b=5:8, eps=0, df.t=NULL)[,-2], 2)
stats::scale
HawMedbb.cll <- glmmTMB::glmmTMB(form, dispformula=~trtGp+splines::ns(scTime,2),
family=glmmTMB::betabinomial(link="cloglog"),
data=HawMed)
devtools::document()
rhub::check()
?rhub
remotes::install_github("r-hub/rhub")
rhub::check()
validate_email()
rhub::validate_email()
whoami()
rhub::whoami()
devtools::whoami()
library(rhub)
install.packages("rhub")
library(rhub)
validate_email()
use_devtools()
library(usethis)
use_devtools()
library(rhub)
validate_email()
library(rhub)
validate_email()
usethis::github_token_help()
usethis::gh_token_help()
gh::gh_whoami()
usethis::gh_token_help()
validate_email()
?validate_email
?check
platforms()
?platforms
library(rhub)
validate_email()
validate_email('john@statsresearch.co.nz')
if(T)T & F
if(T)T & T
install.packages("stylo")
stylo::check,encoding('~/_pkgs/qra/R/')
stylo::check,encoding('~/pkgs/qra/R/')
stylo::check.encoding('~/pkgs/qra/R/')
?iconv
iconvlist()
library(rhub)
validate_email()
?iconv
?tools::showNonASCII
showNonASCII('~/pkgs/qra/R/extractLT/R')
tools::showNonASCII('~/pkgs/qra/R/extractLT/R')
tools::showNonASCII('~/pkgs/qra/R/extractLT/R', file='~/Desktop/out.txt')
tools::showNonASCIIfile('~/pkgs/qra/R/extractLT/R', file='~/Desktop/out.txt')
z <- xfun::read_utf8('~/pkgs/qra/R/extractLT/R')
z <- xfun::read_utf8('~/pkgs/qra/R/extractLT.R')
z
z <- xfun::read_utf8('~/pkgs/qra/R/fieller.R')
z
z <- xfun::read_utf8('~/pkgs/qra/R/fieller.Rd')
z <- xfun::read_utf8('~/pkgs/qra/man/fieller.Rd')
z <- xfun::read_utf8('~/pkgs/qra/man/extractLT.Rd')
validate_email()
stylo::check.encoding('~/pkgs/qra/R/')
stylo::check.encoding('~/pkgs/qra/R/', output='~/Desktop/out.txt')
gh::whoami()
gh::gh_whoami()
gh::print.gh_response()
gh::printgh_response()
gh::gh_whoami()
validate_email()
usethis::edit_r_environ()
